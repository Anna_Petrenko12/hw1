package block4;

public class DistanceInTwoDimensionalSpace {
	public static double calculate(
		    double x1,
		    double y1,
		    double x2,
		    double y2) {

		    double a = Math.abs(y2 - y1);
		    double c = Math.abs(x2 - x1);

		    return Math.hypot(a, c);
		}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(calculate(2,7,5,8));
	}

}
