package block3;

import java.util.Arrays;

public class SortingArray {
	public static void bubblesort(int[] mas) {
		for(int i=mas.length-1;i>0;i--) {
			for(int j=0;j<i;j++) {
				if (mas[j]>mas[j+1]) {
					int tmp=mas[j];
					mas[j]=mas[j+1];
					mas[j+1]=tmp;
				}
			}
		}
		System.out.println(Arrays.toString(mas));
	}
	
	
	public static void selectionsort(int[] mas) {
		for (int i=0; i<mas.length;i++) {
			int minVal=mas[i];
			int minIndex=i;
			
			for (int j=i+1;j<mas.length;j++) {
				if ( mas[j]<minVal) {
					minVal=mas[j];
					minIndex=j;
					
				}
			}
			if (i!=minIndex) {
				int temp=mas[i];
				mas[i]=mas[minIndex];
				mas[minIndex]=temp;
			}
		}
		System.out.println(Arrays.toString(mas));
	}
	
	
	
	
	public static void insertsort(int[] mas) {
		for (int i=0; i<mas.length;i++) {
			int index=i;
			int temp=mas[i];
			while (index>0&&mas[index-1]>=temp) {
				mas[index]=mas[index-1];
				index--;
				
			}
			mas[index]=temp;
		}
		System.out.println(Arrays.toString(mas));
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
	
	int[] A = {5,10,1,9,4,6,3,7};
	 bubblesort(A);
	 selectionsort(A);
	 insertsort(A);
	 
	}

}
